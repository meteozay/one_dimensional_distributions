import os
import numpy as np
import torch
import matplotlib.pyplot as plt


class DensityEstimator:

    def __init__(self, points):
        self.points = points
        self.number_points = len(points)
        self.width = 1.06 * torch.std(points) * self.number_points**(-1/5.)  # simple heuristic
        self.min = np.min(points.detach().numpy())
        self.max = np.max(points.detach().numpy())


    @staticmethod
    def gaussian_kernel(x):
        return 1./np.sqrt(2 * np.pi) * torch.exp(-0.5 * x**2)

    def pdf(self, x):
        return 1./(self.number_points * self.width) * torch.sum(DensityEstimator.gaussian_kernel((x - self.points)/self.width))

    def cdf(self, x):
        return 1./2. + 1/(2 * self.number_points) * torch.sum(torch.erf((x-self.points) / (np.sqrt(2.) * self.width)))

    def get_median(self):
        """
        Finding the median with binary search
        """
        tol = 1e-10
        l = -10. * torch.ones(1).double()
        r = -l
        while (r-l).detach().numpy() > tol:
            mid = (r+l)/2.
            if self.cdf(mid).detach().numpy() >= 0.5:
                r = mid
            else:
                l = mid
        # print(r.detach().numpy(), self.cdf(r).detach().numpy())
        return ((r+l)/2).detach().numpy()

    def plot(self):
        x = np.arange(self.min, self.max, (self.max-self.min)/1000.)

        pdfs = np.zeros(len(x))
        cdfs = np.zeros(len(x))

        for i, elt in enumerate(x):
            pdfs[i] = self.pdf(elt).detach().numpy()
            cdfs[i] = self.cdf(elt).detach().numpy()

        plt.hist(self.points.detach().numpy(), normed=True, bins=int(self.number_points / 50.), color='black', alpha=0.8)
        #plt.plot(x, pdfs)
        #plt.plot(x, cdfs)


if __name__ == '__main__':
    points = np.random.normal(0., 1., size=1000)
    points_torch = torch.from_numpy(points)

    density_estimator = DensityEstimator(points_torch)
    density_estimator.plot()
