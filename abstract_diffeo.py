import os
import numpy as np
import torch
import matplotlib.pyplot as plt
from torch import nn
from torch.utils.data import TensorDataset, DataLoader


class AbstractDiffeo:
    def __init__(self, x_min, x_max):
        self.min = x_min - 2.
        self.max = x_max + 2.

    def see_conditions(self, params=None):
        zero_torch = torch.zeros(1).double()
        if params is None:
            val_zero = self(zero_torch)
            deriv_zero = self.derivative(zero_torch)
        else:
            val_zero = self(zero_torch, params)
            deriv_zero = self.derivative(zero_torch, params)
        print('Value at zero {}, Derivative at zero {}'.format(val_zero.detach().numpy(), deriv_zero.detach().numpy()))

    def plot(self, weights=None):
        points = torch.arange(-1.5, 1.5, 0.05).double()
        values = np.zeros(len(points))
        derivatives = np.zeros(len(points))
        for i, p in enumerate(points):
            if weights is not None:
                values[i] = self(p.unsqueeze(0), weights).detach().numpy()
                derivatives[i] = self.derivative(p, weights).detach().numpy()
            else:
                values[i] = self(p.unsqueeze(0)).detach().numpy()
                #derivatives[i] = self.derivative(p).detach().numpy()

        plt.clf()
        plt.plot(points.detach().numpy(), values)

    def check_derivative(self):
        #without weights:
        for p in np.random.uniform(-2., 2., size=10):
            p_torch = torch.from_numpy(np.array([p]))
            p_torch.requires_grad_(True)
            deriv = self.derivative(p_torch).detach().numpy()
            y = self(p_torch)
            deriv_torch = torch.autograd.grad(y, p_torch)[0].detach().numpy()
            np.allclose(deriv, deriv_torch)

        # with weights:
        weights = torch.zeros_like(self.interpolation_points).normal_().double()
        for p in np.random.uniform(-2., 2., size=10):
            p_torch = torch.from_numpy(np.array([p])).requires_grad_(True)
            deriv = self.derivative(p_torch, weights)
            deriv_torch = torch.autograd.grad(self(p_torch, weights), p_torch)[0]
            deriv_np = deriv.detach().numpy()
            deriv_torch_np = deriv_torch.detach().numpy()
            np.allclose(deriv_np, deriv_torch_np)

    def update(self):
        nb_values = 10000
        step = (self.max - self.min)/nb_values
        points = torch.arange(self.min, self.max, step).double()
        self.values = torch.zeros_like(points)
        self.points = points
        for i, p in enumerate(points):
            self.values[i] = self(p)
        print('Done updating')

    def inverse(self, x, mode='approx'):
        """
        for approx mode, 0-dim torch tensor is expected
        """
        if mode == 'approx':
            return self.inverse_approx(x)
        else:
            return self.inverse_dichotomy(x)

    def inverse_approx(self, x):
        # assert self.min <= x, '{} {}'.format(self.min, x.detach().numpy())
        # assert self.max >= x, '{} {}'.format(self.max, x.detach().numpy())
        # We do linear interpolation. we must find the right index.
        index_1 = torch.argmax(self.values >= x)
        if index_1 == 0:
            return self.points[0]
        else:
            phi_1 = self.values[index_1]
            index_0 = max(index_1 - 1, 0)
            phi_0 = self.values[index_0]
            alpha = (phi_1 - x) / (phi_1 - phi_0)
            return self.points[index_0] * alpha + (1-alpha) * self.points[index_1]

    def inverse_dichotomy(self, x):
        # dichotomy search... efficient and accurate but not differentiable (but the derivative is easy though !)
        tol = 1e-10
        l = self.min * torch.ones(1).double()
        r = self.max * torch.ones(1).double()
        while (r - l).detach().numpy() > tol:
            mid = (r + l) / 2.
            if self(mid).detach().numpy() >= x:
                r = mid
            else:
                l = mid
        return ((r + l) / 2).detach().numpy()

    def plot_metric(self, weights=None, title_size=15):
        step = (self.max - self.min) / 1000
        points = torch.arange(self.min, self.max, step).double()
        values = np.zeros(len(points))
        for i, p in enumerate(points):
            if weights is not None:
                values[i] = self.metric(p.unsqueeze(0), weights).detach().numpy()
            else:
                values[i] = self.metric(p.unsqueeze(0)).detach().numpy()
        plt.clf()
        #plt.title('Computed metric', size=title_size)
        plt.plot(points.detach().numpy(), values, c='black')
        plt.legend()

class Identity(AbstractDiffeo):
    def __init__(self):
        super(Identity, self).__init__()

    def __call__(self, x):
        return x

    def derivative(self, x):
        return torch.ones(1).double()


class Diffeo(AbstractDiffeo):

    def __init__(self, min=-1., max=1., number_points=10):
        super(Diffeo, self).__init__()
        self.number_points = int(number_points)
        self.width = (max - min) / self.number_points
        self.max = max
        self.min = min
        self.interpolation_points = torch.arange(min + self.width/2., max + self.width/2., self.width).double()
        self.shift = 0.
        self.scale_factor = 1.
        self.check_derivative()

    def __call__(self, x, weights):
        weights_ = weights.clamp(min=0.)
        out = self.width * torch.dot(
            weights_,
            torch.atan(self.interpolation_points/self.width) - torch.atan((self.interpolation_points - x)/self.width)
        ) + x
        return (out - self.shift) * self.scale_factor

    def update_mean_and_scale(self, weights):
        weights_ = weights.clamp(min=0.)
        self.shift = 0.
        self.scale_factor = 1.
        zero = torch.zeros(1).double()
        val_0 = self(zero, weights)
        deriv_0 = self.derivative(zero, weights_)
        self.shift = val_0
        self.scale_factor = 1./deriv_0

    def derivative(self, x, weights):
        weights_ = weights.clamp(min=0.)
        return (torch.dot(weights_, 1./(1. + ((self.interpolation_points - x)/self.width)**2)) + 1) * self.scale_factor
