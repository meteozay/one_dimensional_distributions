import os
import numpy as np
import torch
import matplotlib.pyplot as plt
from torch import nn, optim
from torch.utils.data import TensorDataset, DataLoader
from utils import cauchy_density, normal_density, uniform_density, bimodal_samples, bimodal_density, generate_normal_noise

# Try the gradient penalty
def normal_density_unit(x):
    return normal_density(x, scale=1.)


class Generator(nn.Module):

    def __init__(self, in_dim=1, out_dim=1, hidden_dim=32):
        super(Generator, self).__init__()
        self.layers = nn.ModuleList([nn.Linear(in_dim, hidden_dim),
                                     nn.ReLU(),
                                     nn.Linear(hidden_dim, hidden_dim),
                                     nn.ReLU(),
                                     # nn.Linear(hidden_dim, hidden_dim),
                                     # nn.ReLU(),
                                     nn.Linear(hidden_dim, hidden_dim),
                                     nn.ReLU(),
                                     nn.Linear(hidden_dim, out_dim)
                                     ])
        self.double()

    def forward(self, x):
        for layer in self.layers:
            x = layer(x)
        return x


class Critic(nn.Module):

    def __init__(self, in_dim=1, out_dim=1, hidden_dim=32):
        super(Critic, self).__init__()
        self.layers = nn.ModuleList([nn.Linear(in_dim, hidden_dim),
                                     nn.ReLU(),
                                     nn.Linear(hidden_dim, hidden_dim),
                                     nn.ReLU(),
                                     nn.Linear(hidden_dim, hidden_dim),
                                     nn.ReLU(),
                                     nn.Linear(hidden_dim, out_dim)
                                     ])
        self.double()

    def forward(self, x):
        for layer in self.layers:
            x = layer(x)
        return x


def plot_nn(generator, name):
    z_plot = torch.arange(-5., 5., 0.1).double().view(-1, 1)
    images = generator(z_plot).detach().numpy().reshape(-1)
    plt.clf()
    plt.plot(z_plot.detach().numpy(), images)
    plt.savefig(name)


def plot_distribution(generator, target=None):
    z_r = generate_noise(size=(1000, 1))
    images = generator(z_r).detach().numpy().reshape(1000)
    plt.clf()
    plt.hist(images, density=True, bins=50)
    if target is not None:
        z_r = np.arange(np.min(images), np.max(images), 0.05)
        target_densities = [target(elt) for elt in z_r]
        plt.plot(z_r, target_densities)
    plt.savefig('gan_distribution.pdf')


batch_size = 16
nb_iterations = 10001
learning_rate = 1e-4

clamp_lower = -0.1
clamp_upper = 0.1

critic = Critic()
generator = Generator()

optimizer_critic = optim.RMSprop(critic.parameters(), lr=learning_rate)
optimizer_generator = optim.RMSprop(generator.parameters(), lr=learning_rate)

critic_losses = []

torch.manual_seed(0)
np.random.seed(0)


for iteration in range(nb_iterations):
    if iteration == 0:
        n_critic = 500
    else:
        n_critic = 5

    for _ in range(n_critic):
        optimizer_critic.zero_grad()
        optimizer_generator.zero_grad()

        real_data = torch.from_numpy(bimodal_samples(batch_size).reshape(batch_size, 1))

        # real_data = torch.from_numpy(np.random.normal(size=(batch_size, 1)))
        z = generate_normal_noise(size=(batch_size, 1))
        fake_data = generator(z)
        critic_loss = torch.mean(critic(fake_data)) - torch.mean(critic(real_data))
        critic_loss.backward()

        optimizer_critic.step()

        # Clamping
        for p in critic.parameters():
            p.data.clamp_(clamp_lower, clamp_upper)

    optimizer_generator.zero_grad()
    optimizer_critic.zero_grad()

    z = generate_normal_noise(size=(batch_size, 1))
    G_sample = generator(z)
    D_fake = critic(G_sample)
    generator_loss = - torch.mean(D_fake)
    generator_loss.backward()

    optimizer_generator.step()

    if iteration % 10 == 0:
        print('Iteration {} Critic loss {}, generator loss {}'.format(iteration, critic_loss.detach().numpy(), generator_loss.detach().numpy()))

    critic_losses.append(-1.*critic_loss.detach().numpy())

    if iteration % 1000 == 0:
        plot_distribution(generator, target=bimodal_density)
        plot_nn(generator, 'generator.pdf')
        plot_nn(critic, 'discriminator.pdf')


plt.clf()
plt.semilogy(range(nb_iterations), critic_losses)
plt.show()


# We can perform the comparison with what the gan finds
