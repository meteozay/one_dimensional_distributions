import os
import numpy as np
import torch
import matplotlib.pyplot as plt
from math import erf
from tqdm import tqdm


pi_torch = np.pi * torch.ones(1).double()


def jacobian(inputs, outputs):
    return torch.stack([torch.autograd.grad([outputs[i]], [inputs], retain_graph=True, create_graph=True)[0]
                        for i in range(len(outputs))]).detach().numpy()

def get_probability_density(x, phi, mu=0., weights=None):
    """
    phi is an AbstractDiffeo (assumed to be globally diffeomorphic for normalization)
    """
    mu_torch = torch.from_numpy(np.array([mu]))
    if weights is not None:
        y = phi(x, weights)
        image_mu = phi(mu_torch, weights)
        deriv_mu = phi.derivative(mu_torch, weights)
        d = phi.derivative(x, weights)
    else:
        y = phi(x)
        image_mu = phi(mu_torch)
        deriv_mu = phi.derivative(mu_torch)
        d = phi.derivative(x)
    if d.detach().numpy() < 0:
        raise ValueError('I have a negative derivative here {} !'.format(d))
    return torch.exp(-phi.gamma_torch/2. * ((y-image_mu)/deriv_mu)**2) * d /\
           (torch.sqrt(2*pi_torch/phi.gamma_torch) * deriv_mu)


def plot_diffeo(phi, label='', weights=None, plot_inverse=False, title_size=15):
    points = np.arange(phi.min, phi.max, (phi.max-phi.min)/20000)
    if plot_inverse:
        inverses = np.zeros_like(points)

    val = np.zeros_like(points)
    line = np.zeros_like(points)

    for i, p in enumerate(points):
        x = torch.from_numpy(np.array([p]))
        if weights is not None:
            val[i] = phi(x, weights).detach().numpy()
            if plot_inverse:
                inverses[i] = phi.inverse(x, weights).detach().numpy()
        else:
            val[i] = phi(x).detach().numpy()
            if plot_inverse:
                inverses[i] = phi.inverse(x).detach().numpy()
        line[i] = p

    print(val)
    plt.plot(points, val, label=label, c='r')
    plt.plot(points, line, label='identity', c='black', linestyle='dashed')

    if plot_inverse:
        plt.plot(points, inverses, label='Inverse')

    #plt.title('Diffeomorphism', size=title_size)
    plt.legend()


def compute_integral(phi, mu=0., weights=None):
    integral = 0.
    step = 0.05
    points = torch.arange(-10, 10, step).double()
    for p in points:
        integral += get_probability_density(p, phi, mu=mu, weights=weights).detach().numpy()

    return step * integral[0]

def plot_probability_density(phi, mu=0., label='', weights=None, pdf=None, rescale=False, title_size=15):
    step = (phi.max-phi.min)/1000

    points = np.arange(phi.min, phi.max, step)
    integral = 0
    prob = np.zeros_like(points)
    target_density = np.zeros_like(points)

    for i, p in enumerate(points):
        x = torch.from_numpy(np.array([p]))
        prob[i] = get_probability_density(x, phi, mu=mu, weights=weights).detach().numpy()
        integral += step * prob[i]
        if pdf is not None:
            target_density[i] = pdf(p)
    if rescale:
        plt.plot(points, prob/integral, label='density with computed metric', c='blue')
    else:
        plt.plot(points, prob, label='density with computed metric', c='blue')
    if pdf is not None:
        plt.plot(points, target_density, linestyle='dashed', label='target density', c='black')
    #plt.title('Density', size=title_size)
    plt.legend()

    return integral


def inverse_erf_derivative(x):
    if torch.abs(x) > 1.:
        if torch.abs(x) > 1 + 1e-3:
            raise ValueError('argument {} over 1 for inverse erf'.format(torch.abs(x).detach().numpy()))
        else:
            x = torch.clamp(x, -0.999999, 0.999999)
    return torch.sqrt(pi_torch) / 2. * torch.exp(torch.erfinv(x) ** 2)


def cauchy_density(x, gamma=1.):
    return 1./(gamma * np.pi * (1. + (x/gamma)**2))


def cauchy_density_cdf(x, gamma=1.):
    try:
        return 1/np.pi * torch.atan(x/gamma) + 0.5
    except TypeError:
        return 1/np.pi * np.atan(x/gamma) + 0.5


def uniform_density(x):
    try:
        return (torch.floor(x) == 0).double()
    except TypeError:
        return (np.floor(x) == 0).astype(float)


def uniform_density_cdf(x):
    try:
        if x.detach().numpy() < 0:
            return torch.zeros(1).double()
        elif x.detach().numpy() <= 1.:
            return x
        else:
            return torch.ones(1).double()
    except _:
        if x < 0.:
            return 0.
        elif x < 1.:
            return x
        else:
            return 1.


def normal_density(x, mean=0., scale=0.5):
    factor = 1./np.sqrt(2 * np.pi * scale ** 2) * torch.ones(1).double()
    try:
        return factor * torch.exp(-((x-mean)**2/(2 * scale**2)))
    except TypeError:
        return factor * np.exp(-((x-mean)**2/(2*scale**2)))


def normal_density_cdf(x, mean=0., scale=0.5):
    try:
        return 0.5 * (1 + torch.erf((x-mean)/(scale * torch.sqrt(2))))
    except TypeError:
        return 0.5 * (1 + erf((x-mean)/(scale * np.sqrt(2))))


def bimodal_density(x, spread=1.):
    return 0.5 * (normal_density(x, mean=-spread) + normal_density(x, mean=spread))


def bimodal_density_cdf(x, spread=1.):
    return 0.5 * (normal_density_cdf(x, mean=-spread) + normal_density_cdf(x, mean=spread))


def bimodal_samples(length, spread=1.):
    samples_first_mode = np.random.normal(size=length, loc=-spread, scale=0.5)
    samples_second_mode = np.random.normal(size=length, loc=spread, scale=0.5)
    samples_category = np.random.binomial(1, 0.5, size=length)
    return np.multiply(samples_first_mode, samples_category) + np.multiply(samples_second_mode, (1-samples_category))


# Now an extremely complicated distribution:
def multimodal_density(x):
    step = 1.
    points = torch.arange(-3. + step, 3., step).double()
    out = 0.
    for p in points:
        out += normal_density(x, mean=p, scale=0.4 * step)
    return out/len(points)


def multimodal_density_cdf(x):
    step = 1.
    points = torch.arange(-3. + step, 3., step).double()
    out = 0.
    for p in points:
        out += normal_density_cdf(x, mean=p, scale=0.4 * step)
    return out/len(points)


def generate_normal_noise(size):
    return torch.from_numpy(np.random.normal(size=size)).double()


def generate_uniform_noise(size):
    return torch.from_numpy(np.random.uniform(size=size)).double()


def compute_geodesic(a, b, net):
    # a and b are latent positions
    n_points = 50
    n_iters = 2000
    z_traj_np = np.zeros((n_points, len(a)))
    for i in range(n_points):
        z_traj_np[i] = a + i / (n_points-1) * (b - a)
    a_ = torch.from_numpy(a.reshape(1, len(a))).double()
    b_ = torch.from_numpy(b.reshape(1, len(b))).double()
    z_traj_interior = torch.from_numpy(z_traj_np[1:-1]).double()
    z_traj_interior.requires_grad_(True)
    optimizer = torch.optim.Adam([z_traj_interior], lr=1e-3)
    for _ in tqdm(range(n_iters)):
        optimizer.zero_grad()
        complete_trajectory = torch.cat([a_, z_traj_interior, b_])
        trajectory = net(complete_trajectory)
        loss = torch.sum((trajectory[1:] - trajectory[:-1]) **2)
        loss.backward()
        optimizer.step()
    complete_trajectory = torch.cat([a_, z_traj_interior, b_])
    return complete_trajectory.detach().numpy()


def get_embedding_space_positions(net, data, z_dimension):
    position = torch.from_numpy(np.zeros((len(data), z_dimension))).double()
    position.requires_grad_(True)
    n_iters = 1000
    optimizer = torch.optim.Adam([position], lr=1e-3)
    for _ in tqdm(range(n_iters)):
        optimizer.zero_grad()
        loss = torch.sum((net(position) - data)**2)
        loss.backward()
        optimizer.step()
    return position.detach().numpy()


def plot_latent_volume_form(xmin, xmax, ymin, ymax, net):
    xstep = (xmax - xmin) / 10.
    ystep = (ymax - ymin) / 10.
    xx, yy = np.arange(xmin, xmax, xstep), np.arange(ymin, ymax, ystep)
    dets_jac = np.zeros((len(xx), len(yy)))
    for i in range(len(xx)):
        for j in range(len(yy)):
            p = torch.from_numpy(np.array([xx[i], yy[j]])).requires_grad_(True)
            y = net(p)
            jac = jacobian(p, y)
            dets_jac[i, j] = np.sqrt(np.linalg.det(np.matmul(np.transpose(jac), jac)))
    print('Volume:', np.sum(dets_jac) * xstep * ystep)
    plt.clf()
    plt.figure(figsize=(6, 6))
    plt.imshow(dets_jac, interpolation='bilinear', origin='lower')
    plt.xticks(range(len(xx)), [str(elt)[:4] for elt in xx])
    plt.yticks(range(len(yy)), [str(elt)[:4] for elt in yy])
    plt.colorbar()