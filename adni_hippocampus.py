import os
import numpy as np
from datetime import datetime


from csv import reader


adni_merge_path = '/Users/maxime.louis/Documents/ADNI_DATA/Data/ADNIMERGE.csv'

# We load all the found hippocampus volume values and the corresponding ages and save them.

read = reader(open(adni_merge_path, 'r'))

volumes = []
ages = []
rids = []

baseline_dates = {}

# first pass to get the baseline dates:
for i, row in enumerate(read):
    print(i)
    data = row[0].split(';')
    if i == 0:
        rid_index = data.index('RID')
        visit_index = data.index('VISCODE')
        date_index = data.index('EXAMDATE')
    else:
        rid = int(data[rid_index])
        exam_date = datetime.strptime(data[date_index], '%d/%m/%Y')
        viscode = data[visit_index]
        if viscode == 'bl':
            baseline_dates[rid] = exam_date


read = reader(open(adni_merge_path, 'r'))

# first pass to get the baseline dates:
for i, row in enumerate(read):
    print(i)
    data = row[0].split(';')
    if i == 0:
        rid_index = data.index('RID')
        age_index = data.index('AGE')
        hippocampus_index = data.index('Hippocampus')
        date_index = data.index('EXAMDATE')
    else:
        rid = int(data[rid_index])
        exam_date = datetime.strptime(data[date_index], '%d/%m/%Y')
        try:
            volume = float(data[hippocampus_index])
            baseline_age = float(data[age_index])
            print(rid, volume, baseline_age, viscode)
            # Now getting the age:
            age = baseline_age + (exam_date - baseline_dates[rid]).days/356.25
            print(baseline_age, age)
            volumes.append(volume)
            rids.append(rid)
            ages.append(age)
        except ValueError:
            continue






for (age, vol, i) in zip(ages, volumes, rids):
    print(age, vol, i)

ages = np.array(ages, dtype=float)
volumes = np.array(volumes, dtype=float)
rids = np.array(rids, fmt='%d')









np.savetxt('ages.txt', ages)
np.savetxt('volumes.txt', volumes)
np.savetxt('rids.txt', rids)